// 2-------------------
// this

const counter = {
    count: 5,
    add() {
        console.log(this.count + 1);
    },
    reduce() {
        console.log(this.count - 1);
    },
    init(num) {
        this.count = num
    }
}

counter.init(2)
counter.add()
counter.reduce()

// 1-----------------------
// this и ptototype

const BBF = {
    company: "BBF",
    country: "Italia",
    getGuarantee: function () { return this.age <= 4 }
}
const BCF = {
    company: "BCF",
    country: "Chine",
    getGuarantee: function () { return this.age <= 2 }
}

function refund() {
    if (this.isNew) {
        console.log("нечего менять, все и так новое")
        return
    }
    this.getGuarantee() ? this.isNew = true : console.log("мебель уже слишком старая, не выйдет вернуть")
}

const sofa = {
    type: "sofa",
    age: 3,
    isNew: false,
    refund, // если названия поля совпадает с переменной, то можно просто так писать сразу
    __proto__: BBF
}

const table = {
    type: "table",
    age: 3,
    isNew: false,
    refund,
    __proto__: BCF
}

console.log(table.refund())

// 3------------------------

const barn = {
    count: 5,
    maxCount: 10,
}

const worker = {
    isKing: false,
    add: function () {
        if (this.count >= this.maxCount) {
            console.log("амбар полон")
            return
        }
        this.count = this.count + this.strong
        if (this.count >= this.maxCount) {
            this.isKing = true
        }
    },
    __proto__: barn
}

const workerMax = {
    Name: "Max",
    strong: 2,
    __proto__: worker
}

const workerDima = {
    Name: "Dima",
    strong: 3,
    __proto__: worker
}

console.log(workerMax.add())

// 4-----------------------------

class Firm {
    constructor(company, country, guaranteeAge) {
        this.company = company;
        this.country = country;
        this.guaranteeAge = guaranteeAge;
    }

    isCanGuarantee = ({ isRefund, age }) => {
        return !isRefund && age <= this.guaranteeAge;
    };
}

class Furniture {
    constructor(type, age, firm) {
        this.type = type;
        this.age = age;
        this.firm = firm;
        this.isRefund = false;
    }

    refund = () => {
        this.firm.isCanGuarantee(this)
            ? (this.isRefund = true)
            : console.log('мебель уже слишком старая, не выйдет вернуть');
    };
}

const firmBCF = new Firm('BCF', 'EN', 2);
const firmBBF = new Firm('BBF', 'RU', 4);

const table1 = new Furniture('table', 2, firmBCF);
const table2 = new Furniture('table', 2, firmBCF);
const table3 = new Furniture('table', 2, firmBBF);